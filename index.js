const path = require(`path`)

const { browserify } = require(`@suzan_pevensive/browserify-js`)

module.exports = {

  when: require(`./src/when`),

  browserify: (app) => browserify.publishModule(app, `WhenJs`, path.join(__dirname, `src`), `js`)

}
