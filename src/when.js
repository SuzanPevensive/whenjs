/**

  @description
    return result of correct condition or else condition

  @param {any}    value - value implemented in conditions
  @param {array}  conditions - array of conditions for value
                               should contains "else" key
  @return {any}

  @example "for number"

    let value = 12
    let result = when(typeof value, {
                  number: () => `is number`,
                  string: () => `is string`,
                  else: () => `is any other`
                })
    result == `is number`

  @example "for string"

    let value = `12`
    let result = when(typeof value, {
                  number: () => `is number`,
                  string: () => `is string`,
                  else: () => `is any other`
                })
    result == `is string`

  @example "else"

    let value = true
    let result = when(typeof value, {
                  number: () => `is number`,
                  string: () => `is string`,
                  else: () => `is any other`
                })
    result == `is any other`

*/
module.exports = function(value, conditions){
  for(let key in conditions){
    let condition = conditions[key]
    let conditionKey = typeof condition === `object` && condition != null ? condition.key || key : key
    let conditionResult = typeof condition === `object` && condition != null ? condition.result || condition : condition
    if(typeof conditionKey == `function`){
      if(conditionKey(key, value)) return conditionResult()
    }else if(conditionKey == value) return conditionResult()
  }
  return conditions["else"]();
}
